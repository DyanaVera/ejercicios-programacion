Variables
	longitud_lado float
        Perímetro float
	error string
    lados = 3
    longitud_lado = 0
    error = "La longitud del lado del triángulo debe ser mayor que 0:"
    resultado = "El perímetro del triángulo equilátero es:"
    perímetro = longitud_lado * Lados

Inicio
    Escribir "Ingresa la longitud del lado del triángulo:"
    Leer longitud_lado
	Si (longitud_lado >= 0.1) Entonces
	    Escribir resultado, perímetro
	Sino
	    Escribir error
	FinSi
Fin 